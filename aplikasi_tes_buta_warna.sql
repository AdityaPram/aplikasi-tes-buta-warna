-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Jun 2020 pada 05.23
-- Versi server: 10.1.39-MariaDB
-- Versi PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aplikasi_tes_buta_warna`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id_pertanyaan` int(10) NOT NULL,
  `pertanyaan` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `jawaban` int(10) NOT NULL,
  `pilihan_a` int(10) NOT NULL,
  `pilihan_b` int(10) NOT NULL,
  `pilihan_c` int(10) NOT NULL,
  `pilihan_d` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pertanyaan`
--

INSERT INTO `pertanyaan` (`id_pertanyaan`, `pertanyaan`, `gambar`, `jawaban`, `pilihan_a`, `pilihan_b`, `pilihan_c`, `pilihan_d`) VALUES
(1, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar1.jpg', 2, 3, 19, 2, 7),
(2, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar2.jpg', 9, 5, 16, 30, 9),
(3, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar3.jpg', 8, 8, 12, 0, 6),
(4, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar4.jpg', 3, 8, 9, 3, 1),
(5, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar5.jpg', 7, 10, 1, 6, 7),
(6, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar6.jpg', 0, 0, 17, 9, 14),
(7, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar7.jpg', 1, 3, 1, 6, 7),
(8, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar8.jpg', 5, 9, 5, 28, 12),
(9, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar9.jpg', 2, 3, 2, 12, 20),
(10, 'Apa yang kamu lihat dari gambar di atas ?', 'image/gambar10.jpg', 6, 17, 3, 9, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_perhitungan`
--

CREATE TABLE `temp_perhitungan` (
  `id_perhitungan` int(10) NOT NULL,
  `jumlah_pertanyaan_selesai` int(10) NOT NULL,
  `jumlah_pertanyaan_benar` int(10) NOT NULL,
  `persentase_parsial` int(10) NOT NULL,
  `persentase_buta_warna` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `temp_perhitungan`
--

INSERT INTO `temp_perhitungan` (`id_perhitungan`, `jumlah_pertanyaan_selesai`, `jumlah_pertanyaan_benar`, `persentase_parsial`, `persentase_buta_warna`) VALUES
(1, 10, 10, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_pertanyaan`
--

CREATE TABLE `temp_pertanyaan` (
  `id` int(10) NOT NULL,
  `pertanyaan_aktif` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `temp_pertanyaan`
--

INSERT INTO `temp_pertanyaan` (`id`, `pertanyaan_aktif`) VALUES
(1, 11);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id_pertanyaan`);

--
-- Indeks untuk tabel `temp_perhitungan`
--
ALTER TABLE `temp_perhitungan`
  ADD PRIMARY KEY (`id_perhitungan`);

--
-- Indeks untuk tabel `temp_pertanyaan`
--
ALTER TABLE `temp_pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id_pertanyaan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `temp_perhitungan`
--
ALTER TABLE `temp_perhitungan`
  MODIFY `id_perhitungan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `temp_pertanyaan`
--
ALTER TABLE `temp_pertanyaan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
